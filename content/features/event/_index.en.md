---
title: Event Handling - The Event Bus
weight: 11
---

Everything in the Mochi Engine evokes events on the EventBus.

It is also used for message passing between components.

## Event Bus
The Event Bus class can be used outside of any component for event systems:

```javascript
let events = new Mochi.Events.EventBus();
// you can subscribe
events.subscribe("my_event", function(data) {
    // my event callback
});
// you can broadcast events
events.broadcast("my_event", { data:"hello" });
```