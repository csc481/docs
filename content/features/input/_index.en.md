---
title: Input - Callback or State
weight: 10
---

Input provides mouse or keyboard information. It has two different styles which allows
multiple games to handle input how they want.

All GameStates manage their own InputHandler, which can be used to detect input.
When the GameState swaps, all input bindings are saved and restored when the state is brought back.

## Input (Callback)

Callbacks provide events for when keys or mice are pressed.

```javascript
// Mouse events:
game.input.onMouseDown(function(x, y) {
    // do something
}));
// key events
game.input.onKeyDown("w", function(evt) {
    // do something
});
```

## Input (state)

Alternatively, the InputHandler maintains a set of variables based on the state of what is pressed.

These can be used in an Update loop to determine if they are down or not.

For example, in Asteroids:

```javascript
// Input is attached to game.input
this.input = game.input
// ....
update(dt) {
    this.elapsedTime += dt;
    if ('a' in this.input.keysDown) {
        this.turnLeft(dt);
    }
    if ('d' in this.input.keysDown) {
        this.turnRight(dt);
    }
    if ('w' in this.input.keysDown) {
        this.accelerate(dt);
    }
    if ('s' in this.input.keysDown) {
        this.decelerate(dt);
    }
    if (' ' in this.input.keysDown) {
        this.fire();
    }
}
```