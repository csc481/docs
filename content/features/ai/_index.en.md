---
title: Artificial Intelligence - Pathfinding
weight: 13
---

The AI in Mochi is somewhat small and reduced just to PathFinding.

## Pathfinding - A*

The A Star algorithm has been implemented for basic pathfinding. Here is an example from
Cops & Robbers:

```javascript
let thisPath = Mochi.Ai.PathFinding.aStar(map, [this.entity.x, this.entity.y], [characters[i].x, characters[i].y]);
// returns a path of coordinates to navigate on
```