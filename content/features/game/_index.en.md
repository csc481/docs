---
title: Game & State Management
weight: 5
---

The Mochi Engine has a system for managing games, through the Game class.

## The Game Class

The Game class allows you to create framerate-update-independent games with a desired framerate for rendering easily.
The Game class works with the GameState class, which allows you to add additional states and swap between them.

## The Game State Class

The Game State class allows you to add new states to games. States provide easy ways to switch between what's currently
occurring in a game.

Here's a skeleton Game State:

```javascript
class ExampleState extends GameState {
    constructor(name) {
        super(name);
    }

    /** Called once when the game initializes */
    init (game, states) {}

    /** Whenever this state is entered each time, this is called */
    enter (game, states, lastState) {}

    /** Called whenever the game wants to update */
    update(ms) {}

    /** Called whenever the game wants to try */
    draw (ctx) {}
}
```

GameStates can also be created using a bootstrapping existing state with the setup method.

This can be useful for when you want a simple state without creating yet another class, or making a strange dynamic game state.

```javascript
let sampleState = new Mochi.GameState("myState").setup(
    function(ms) {}, //update
    function(ctx) {}, //draw
    function(game, states) {}, //init
    function(game, states, lastState) {} //enter
)
```

## Provided Game States

The Mochi Engine comes with several game states you can include right out of the box.

The intention was to quickly create new games.

 * IntroState - Used for an intro with the game name
 * LoadState  - Used for loading the game
 * PauseState - Used for adding in the ability to pause
 * EndState - Used for an end screen, potentially with a score.