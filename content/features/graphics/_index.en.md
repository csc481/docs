---
title: Graphics - Sprites, User Interface & Animation
weight: 8
---

The Graphics class provides sprites, animated sprites, and user interface components.

## Sprites

The Sprite class is a component that can be added to any entity. When it is combined with a DrawManager
it draws images at the entity location.

Sprites will accept rotation if provided on entity.

## Draw Manager

The DrawManager class can be added to a Scene. It will draw any entity in the scene that has a sprite component.

The UnitDrawManager can be used in situations when you want to apply a grid to the world first.

## Animated Sprites

AnimatedSprite works exactly like the Sprite component except it requires a sprite sheet and animation information.

## User Interface

There are a set of different components that can be added to a GameState that allows easy UI development.

* Element - Allows you to extend different UI components and make your own
* Background - draws a select color or image on the background of the screen
* Select - Allows to choose between an indeterminate amount of options
* Text - Allows text to be drawn at a specified location.