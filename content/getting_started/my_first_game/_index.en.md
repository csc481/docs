---
date: 2016-04-09T16:50:16+02:00
title: Hello World Game
weight: 20
---

## Structure

The Mochi Engine intentionally allows users to freely create whatever structure they desire. 
However, the engine must be required using [requirejs](http://requirejs.org/).

We recommend a structure similar to this for **single player**:

```
projects/
    engine/
        ....
        -> Mochi.js
    helloworld/
        assets/
        data/
        -> index.html // Open to run game
        -> main.js // the entrypoint into the game
```

And for multiplayer..

```
projects/
    engine/
        ....
        -> MochiServer.js
        -> Mochi.js
    helloworld/
        assets/
        data/
        -> index.html // Open to run game
        -> main.js // the entrypoint into the game
        -> server.js // the entrypoint for the game server
```

## Bootstrapping index.html

Here is a convenient index.html template that can be used for most games made with Mochi.

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>Hello World Game</title>
    <style>
        /* For convenience */
        html, body {
            width:  100%;
            height: 100%;
            margin: 0px;
        }
    </style>
  </head>
  <body>
    <!-- main.js contains all the game and the actual main loops! Start in main.js!!! Uses require from index.html -->
	<script data-main="main.js" src="../engine/libraries/require.js"></script>
  </body>
</html>

```

## Acquiring the Engine

The Engine can be easily required using requirejs relatively in, for example, a *main.js*:

```javascript
// main.js
requirejs('../engine/Mochi', function(Mochi) {

    // your game code
    ....

});
```

{{% notice tip %}}
The Mochi file contains all packages and dependencies needed to use any of Mochi's functionality.
It uses a full namespace to access any of the variables.
{{% /notice %}}

## The Game class

The Mochi Game class will maintain a game state that has an update and render loop. It renders the loop at the specified framerate when possible,
and updates on a consistent interval. This state can transition into other states.

```javascript
// main.js
requirejs('../engine/Mochi', function(Mochi) {

    class HelloWorldGame {
        constructor() {
            // creates a new game in mochi
            this.game = new Mochi.Game({
                // make it the width/height of the window
                width: window.innerWidth,
                height: window.innerHeight,
                // we want a desired 60 fps
                fps: 60,
                // the first game state to load
                startState: "main"
            });
            
            // let's add a gamestate to this game
            this.game.addState(
                // instead of extending a game state, we can just pass in the functions we want it to run like one
                new Mochi.GameState("main").setup(this.update.bind(this), this.draw.bind(this), function() {}, function() {})
            );
        }
        
        // the update loop
        update(ms) {
            console.log("updating!");
        }
        
        // the rendering loop given an html5 context
        draw(ctx) {
            console.log("Draw!");
        }
        
        // convenience function to run the game!
        start() {
            this.game.start();
        }
    }
    
    // create an instance of this game
    var helloWorld = new HelloWorldGame();
    helloWorld.start();
});
```

{{% notice tip %}}
The Mochi Engine favors composition over inheritance. We recommend owning an instance of *game* instead of extending it!
{{% /notice %}}

Open up *index.html* and the simple game is running!