---
date: 2016-04-09T16:50:16+02:00
title: Hello World Server
weight: 25
---

## Structure

The Mochi Engine will let you organize your code however you want, but we recommend:

```
projects/
    engine/
        ....
        -> MochiServer.js
        -> Mochi.js
    helloworld/
        assets/
        data/
        -> index.html // Open to run game
        -> main.js // the entrypoint into the game
        -> server.js // the entrypoint for the game server
```

## Bootstrapping server.js

Since you've already set up a client (in Hello World Game), you can create a simple game server
easily using Mochi's GameServer class.

The Mochi GameServer provides all the components to make a working server.

```javascript
// Get the Mochi Server version.
var Mochi = require('../engine/MochiServer');
// Creating a Mochi game server on port 3000
let server = new Mochi.Server.GameServer(3000);
// set up a fully working web server
server.setupWebServer();

// the server contains whatever components you need to make a server
var app = server.app;
var path = server.path;
var http = server.http;
var express = server.express;
var io = server.io;

// Give main client page, index.html
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});
// expose the engine files to be acquired
app.use('/engine/', express.static(path.join(__dirname, '..', 'engine')));

// expose our game files, like main.js
app.use('/', express.static(__dirname));

// tell our server to listen
server.listen();
```

Now you can start up the server using node:

```
node server.js
```

And navigate your browser to localhost:3000. It will send you index.html.