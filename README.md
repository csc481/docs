# Documentation for Mochi Engine

The Hugo engine is used to power creating this documentation.

# Setup

In order to run this documentation web server, you need to run [hugo](https://gohugo.io/getting-started/installing).

Just go to the docs directory and type:
```
hugo serve
```

And go to localhost:1313